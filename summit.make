core = 7.x
api = 2

projects[] = drupal
projects[] = addressfield
projects[] = admin_menu
projects[] = backup_migrate
projects[] = block_class
projects[] = captcha
projects[] = context
projects[] = ctools
projects[] = custom_breadcrumbs
projects[] = date
projects[] = defaultcontent
projects[] = entity
projects[] = features
projects[features_extra][version] = "1.x-dev"
projects[] = fences
projects[] = field_group
projects[] = file_entity
projects[] = fullcalendar
projects[] = geocoder
projects[] = geofield
projects[] = geophp
projects[] = google_analytics
projects[] = i18n
projects[] = imce
projects[] = imce_wysiwyg
projects[] = imagefield_crop
projects[] = jquery_update
projects[] = libraries
projects[] = link
projects[login_security][version] = "1.x-dev"
projects[] = migrate
projects[] = menu_block
projects[] = menu_breadcrumb
projects[oembed][version] = "0.x-dev"
projects[oembedthumbnail][version] = "0.1"
projects[] = openlayers
projects[] = pathauto
projects[] = re_contextlibraries
projects[remote_stream_wrapper][version] = "1.0-beta3"
projects[remote_stream_wrapper][patch][] = "http://drupal.org/files/0001-Bug-1444626-Module-should-work-with-Media-module-dis_0.patch"
projects[] = references
projects[] = rss_field_formatters
projects[semanticviews][version] = "1.x-dev"
projects[] = strongarm
projects[] = token
projects[] = uuid
projects[] = variable
projects[] = views
projects[] = webform
projects[wysiwyg][version] = "2.1"
projects[wysiwyg][patch][] = "http://drupal.org/files/0001-feature.inc-from-624018-211.patch"


; Libraries - Uncomment as you need

;libraries[superfish][download][type] = "git"
;libraries[superfish][download][url] = "git@github.com:mehrpadin/Superfish-for-Drupal.git"
;libraries[superfish][directory_name] = "superfish"
;libraries[superfish][type] = "library"

libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://cloud.github.com/downloads/tinymce/tinymce/tinymce_3.5.6.zip"
libraries[tinymce][directory_name] = "tinymce"
libraries[tinymce][type] = "library"

libraries[foundation][download][type] = "get"
libraries[foundation][download][url] = "http://apps.raisedeyebrow.com/cogito/foundation.zip"
libraries[foundation][directory_name] = "foundation"
libraries[foundation][type] = "library"

libraries[fullcalendar][download][type] = "get"
libraries[fullcalendar][download][url] = "http://apps.raisedeyebrow.com/fullcalendar.zip"
libraries[fullcalendar][directory_name] = "fullcalendar"
libraries[fullcalendar][type] = "library"


; Themes 

; Admin theme
projects[] = rubik
projects[] = tao

; Cogito
projects[cogito][download][type] = "git"
projects[cogito][download][url] = "http://git.drupal.org/project/cogito.git"
;"git@git.drupal.org:project/cogito.git"
projects[cogito][download][branch] = "7.x-1.x"
projects[cogito][type] = "theme"


; Custom Modules - Uncomment as you need

projects[re_block_width][download][type] = "git"
projects[re_block_width][download][url] = "https://bitbucket.org/reyebrow/re_block_width.git"
projects[re_block_width][download][branch] = "7.x-1.x"
projects[re_block_width][subdir] = "re_custom"
projects[re_block_width][type] = "module"

projects[re_orbit][download][type] = "git"
projects[re_orbit][download][url] = "https://bitbucket.org/reyebrow/re_orbit.git"
projects[re_orbit][download][branch] = "7.x-1.x"
projects[re_orbit][subdir] = "re_custom"
projects[re_orbit][type] = "module"

projects[re_social_media][download][type] = "git"
projects[re_social_media][download][url] = "https://bitbucket.org/reyebrow/re_social_media.git"
projects[re_social_media][download][branch] = "7.x-1.x"
projects[re_social_media][subdir] = "re_custom"
projects[re_social_media][type] = "module"

; Features - Uncomment as you need

projects[re_wysiwyg_profile][download][type] = "git"
projects[re_wysiwyg_profile][download][url] = "https://bitbucket.org/reyebrow/re_wysiwyg_profile.git"
projects[re_wysiwyg_profile][download][branch] = "7.x-1.x"
projects[re_wysiwyg_profile][subdir] = "re_features"
projects[re_wysiwyg_profile][type] = "module"

projects[re_setup][download][type] = "git"
projects[re_setup][download][url] = "https://bitbucket.org/reyebrow/re_setup.git"
projects[re_setup][download][branch] = "7.x-1.x"
projects[re_setup][subdir] = "re_features"
projects[re_setup][type] = "module"

projects[re_roles][download][type] = "git"
projects[re_roles][download][url] = "https://bitbucket.org/reyebrow/re_roles.git"
projects[re_roles][download][branch] = "7.x-1.x"
projects[re_roles][subdir] = "re_features"
projects[re_roles][type] = "module"

projects[re_videos][download][type] = "git"
projects[re_videos][download][url] = "https://bitbucket.org/reyebrow/re_videos.git"
projects[re_videos][download][branch] = "7.x-1.x"
projects[re_videos][subdir] = "re_features"
projects[re_videos][type] = "module"

projects[re_blog][download][type] = "git"
projects[re_blog][download][url] = "https://bitbucket.org/reyebrow/re_blog.git"
projects[re_blog][download][branch] = "7.x-1.x"
projects[re_blog][subdir] = "re_features"
projects[re_blog][type] = "module"

projects[re_events][download][type] = "git"
projects[re_events][download][url] = "https://bitbucket.org/reyebrow/re_events.git"
projects[re_events][download][branch] = "7.x-1.x"
projects[re_events][subdir] = "re_features"
projects[re_events][type] = "module"

projects[re_slideshow][download][type] = "git"
projects[re_slideshow][download][url] = "https://bitbucket.org/reyebrow/re_slideshow.git"
projects[re_slideshow][download][branch] = "7.x-1.x"
projects[re_slideshow][subdir] = "re_features"
projects[re_slideshow][type] = "module"

projects[re_page][download][type] = "git"
projects[re_page][download][url] = "https://bitbucket.org/reyebrow/re_page.git"
projects[re_page][download][branch] = "7.x-1.x"
projects[re_page][subdir] = "re_features"
projects[re_page][type] = "module"
